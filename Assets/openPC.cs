﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class openPC : MonoBehaviour {
	public GameObject openedCanvas;
	public GameObject hintText;
	public GameObject player;
	bool spacePressed;
	// Use this for initialization
	private void OnTriggerEnter(Collider other)
	{
		if( other.tag=="Player" && !hintText.activeSelf)
		{
			spacePressed = false;
			hintText.SetActive(true);
		}

	}

	private void OnTriggerStay(Collider other)
	{
		if (other.tag == "Player" && !spacePressed && Input.GetKey(KeyCode.Space))
		{
			spacePressed = true;
			hintText.SetActive(false);
			openedCanvas.SetActive(true);

			player.GetComponent<moveAndRotate>().disableMove();
		}

		if (other.tag == "Player" && spacePressed && Input.GetKey(KeyCode.Escape))
		{
			spacePressed = false;
			hintText.SetActive(false);
			openedCanvas.SetActive(false);

			player.GetComponent<moveAndRotate>().enableMove();
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player" && hintText.activeSelf)
		{
			hintText.SetActive(false);
		}
		if (other.tag == "Player" && openedCanvas.activeSelf)
		{
			openedCanvas.SetActive(false);
		}
	}
}
