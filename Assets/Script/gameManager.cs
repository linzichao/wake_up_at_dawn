﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameManager : MonoBehaviour {
    public int totalLevelCount = 1;
    public int initLevel = 1;

    public GameObject player;
    public GameObject camera;

    public void stopPlayer()
    {
        player.GetComponent<moveAndRotate>().enabled = false;
        camera.GetComponent<cameraMove>().enabled = false;
    }
    public void resumePlayer()
    {
        player.GetComponent<moveAndRotate>().enabled = true;
        camera.GetComponent<cameraMove>().enabled = true;
    }

    void Start()
    {
        //startLevel(initLevel);
    }

    public void startLevel(int level)
    {
        Level tmp = gameObject.GetComponent("level_" + level) as Level;
        tmp.enabled = true;
    }
    public void completeLevel(int level)
    {
        Level tmp = gameObject.GetComponent("level_" + level) as Level;
        tmp.enabled = false;
        print("level = " + level + ", total = " + totalLevelCount);
        if(level == totalLevelCount)
        {
            /// TODO
            print("done game!!!");
        }
        else
        {
            startLevel(level + 1);
        }

    }
    
}
