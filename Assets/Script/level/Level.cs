﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Level: MonoBehaviour {
    public List<GameObject> activeWhenStart = new List<GameObject>();   //list to enabled when start level
    public List<GameObject> inactiveWhenStart = new List<GameObject>(); //list to disabled when start level
    public List<GameObject> activeWhenComplete = new List<GameObject>();   //list to enabled when complete level
    public List<GameObject> inactiveWhenComplete = new List<GameObject>(); //list to disabled when complete level

    // Use this for initialization
    public abstract void LevelStart();
    void Start ()
    {
        print(this.GetType().Name+" start");    //console which level is going to start
        foreach ( GameObject obj in activeWhenStart) {
            obj.SetActive(true);
        }
        foreach (GameObject obj in inactiveWhenStart)
        {
            obj.SetActive(false);
        }
        LevelStart();
    }
    // Update is called once per frame
    public abstract void LevelUpdate();
    private void Update()
    {
        LevelUpdate();
    }

    public abstract void LevelComplete();
    private void OnDisable()
    {
        LevelComplete();
    }
}
