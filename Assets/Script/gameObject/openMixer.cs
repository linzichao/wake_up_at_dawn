﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class openMixer : MonoBehaviour {

    public GameObject openedCanvas;
    public GameObject openedCanvas_with_hands;
    public GameObject hintText;
    public GameObject hintText_with_hands;
    public GameObject hand;

    bool spacePressed;
    bool spacePressed2;

    // Use this for initialization
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !hintText.activeSelf && !hand.activeSelf)
        {
            spacePressed2 = false;
            hintText.SetActive(true);
        }
        else if(other.tag == "Player" && !hintText_with_hands.activeSelf && hand.activeSelf)
        {
            spacePressed = false;
            spacePressed2 = true;
            hintText_with_hands.SetActive(true);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && !spacePressed && Input.GetKey(KeyCode.Space) && hand.activeSelf)
        {
            spacePressed = true;
            hintText_with_hands.SetActive(false);
            openedCanvas_with_hands.SetActive(true);
        }
        if (other.tag == "Player" && !spacePressed2 && Input.GetKey(KeyCode.Space))
        {
            spacePressed = true;
            hintText.SetActive(false);
            openedCanvas.SetActive(true);
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" && hintText.activeSelf)
        {
            hintText.SetActive(false);
        }

        if (other.tag == "Player" && openedCanvas.activeSelf)
        {
            openedCanvas.SetActive(false);
        }

        if (other.tag == "Player" && hintText_with_hands.activeSelf)
        {
            hintText_with_hands.SetActive(false);
        }

        if (other.tag == "Player" && openedCanvas_with_hands.activeSelf)
        {
            openedCanvas_with_hands.SetActive(false);
        }

    }

}
