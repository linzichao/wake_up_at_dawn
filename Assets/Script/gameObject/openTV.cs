﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class openTV : MonoBehaviour
{

    public GameObject hintText;
    public GameObject noDVDHintText;
    public GameObject maincamera;
    public GameObject DVD;
    bool spacePressed;

    private void OnTriggerEnter(Collider other)
    {
        spacePressed = false;
        hintText.SetActive(true);
    }
    private void OnTriggerStay(Collider other)
    {
        if (!spacePressed && Input.GetKey(KeyCode.Space))
        {
            hintText.SetActive(false);
            spacePressed = true;
            if (DVD.activeSelf)
            {
                VideoPlayer videoplayer = maincamera.GetComponent<VideoPlayer>();
                videoplayer.Play();
            }
            else
            {
                noDVDHintText.SetActive(true);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        hintText.SetActive(false);
        if (noDVDHintText) noDVDHintText.SetActive(false);
    }
}
