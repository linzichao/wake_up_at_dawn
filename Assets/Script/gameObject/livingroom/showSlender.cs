﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class showSlender : MonoBehaviour {
    public GameObject playerLight;
    public Color slenderEnvironmentColor;
    public GameObject previousCanbinetTrigger;
    public GameObject afterCanbinetTrigger;

    Color prevEnviromentColor;

    private void Start()
    {
        Destroy(previousCanbinetTrigger);
        afterCanbinetTrigger.SetActive(true);
    }

    private void OnEnable()
    {
        //print("enabled!");
        prevEnviromentColor = RenderSettings.ambientLight;
        RenderSettings.ambientLight = slenderEnvironmentColor;
        playerLight.SetActive(false);
    }
    private void OnDisable()
    {
        //print("disabled");
        RenderSettings.ambientLight = prevEnviromentColor;
        playerLight.SetActive(true);

    }
}
