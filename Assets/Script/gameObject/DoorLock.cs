﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class DoorLock : MonoBehaviour
{
    public GameObject hintText;
    public GameObject DoorCodeConsole;

    public GameObject rotateObject;
    public GameObject colliderObject;
    Collider doorCollider;

    public float closedAngle;
    public float openedAngle;
    public float rotateSpeed;

    public Flowchart doorFlowchart;

    bool openDoor = false;
    float goalAngle;

    // Use this for initialization
    void Start()
    {
        doorCollider = colliderObject.GetComponent<Collider>();
        goalAngle = rotateObject.transform.localEulerAngles.y;
    }
    void OnTriggerEnter(Collider other)
    {
        if (!openDoor)
        {
            hintText.SetActive(true);
            Block targetBlock = doorFlowchart.FindBlock("DoorFool");
            doorFlowchart.ExecuteBlock(targetBlock);
        }else{

            hintText.SetActive(true);

        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKey(KeyCode.Space) && hintText.activeSelf)
        {
            if (openDoor)
            {
                hintText.SetActive(false);
                goalAngle = openedAngle;
                doorCollider.enabled = false;
            }
            else
            {
                DoorCodeConsole.SetActive(true);
            }
        }
        else if (Input.GetKey(KeyCode.Escape))
        {
            if( DoorCodeConsole ) DoorCodeConsole.SetActive(false);
        }
    }

    public void openTheDoor(){

        openDoor = true;
        hintText.SetActive(false);
        goalAngle = openedAngle;
        doorCollider.enabled = false;
        Destroy(DoorCodeConsole);
        Destroy(doorFlowchart.gameObject);

    }

    private void OnTriggerExit(Collider other)
    {
        if (!hintText.activeSelf) goalAngle = closedAngle;
        else hintText.SetActive(false);
    }
    void Update()
    {
        Vector3 objectAngle = rotateObject.transform.localEulerAngles;
        if (Mathf.Abs(objectAngle.y - goalAngle) > 0.1)
        {
            //sound there
            float ang = rotateSpeed * (goalAngle - objectAngle.y > 0 ? 1 : -1);
            rotateObject.transform.localEulerAngles = new Vector3(objectAngle.x, objectAngle.y + ang, objectAngle.z);
        }
        else if (!doorCollider.enabled)
        {
            doorCollider.enabled = true;
        }
    }
}
