﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class openDoor : MonoBehaviour
{
    public GameObject hintText;

    public GameObject rotateObject;
    public GameObject colliderObject;
    public Collider doorCollider;

    public float closedAngle;
    public float openedAngle;
    public float rotateSpeed;

    float goalAngle;

    // Use this for initialization
    void Start () {
        doorCollider = colliderObject.GetComponent<Collider>();
        goalAngle = rotateObject.transform.localEulerAngles.y;
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            hintText.SetActive(true);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && Input.GetKey(KeyCode.Space) && hintText.activeSelf)
        {
            hintText.SetActive(false);
            goalAngle = openedAngle;
            doorCollider.enabled = false;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!hintText.activeSelf) goalAngle = closedAngle;
            else hintText.SetActive(false);
        }
    }
    void Update ()
    {
        Vector3 objectAngle = rotateObject.transform.localEulerAngles;
        if (Mathf.Abs(objectAngle.y - goalAngle) > 0.1)
        {
            //sound there
            float ang = rotateSpeed * (goalAngle - objectAngle.y > 0 ? 1 : -1);
            rotateObject.transform.localEulerAngles = new Vector3(objectAngle.x, objectAngle.y + ang, objectAngle.z);
        }
        else if( !doorCollider.enabled )
        {
            doorCollider.enabled = true;
        }
    }
}
