﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cursorWhenMouseEnter : MonoBehaviour {
    public Texture2D cursor;
    void OnMouseEnter()
    {
        Cursor.SetCursor(cursor, Vector2.zero, CursorMode.Auto);
    }

    public void OnMouseExit()
    {
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }
    public void destroySelf()
    {
        Destroy(gameObject);
    }
}
