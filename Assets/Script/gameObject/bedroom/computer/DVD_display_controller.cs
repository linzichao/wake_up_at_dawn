﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DVD_display_controller : MonoBehaviour {
	
	public GameObject display;
	public GameObject icon;
	public GameObject menu;
	public GameObject eject;


	// Use this for initialization
	void Start () {
		icon.SetActive (true);
		menu.SetActive (false);
		eject.SetActive (false);
	}

	// Update is called once per frame
	void Update () {
		if( Input.GetKey(KeyCode.Escape) ) {
			display.SetActive (false);
			icon.SetActive (false);
			menu.SetActive (false);
			eject.SetActive (false);
		}
	}
		
}
