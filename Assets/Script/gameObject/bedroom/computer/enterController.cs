﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class enterController : MonoBehaviour {
	
	public GameObject login;
	public InputField pwd;
	public GameObject pwd_object;
	public GameObject enter;
	public GameObject head;
	public AudioSource bgmSource;

	public GameObject desktop;
	public GameObject icon;
	public GameObject txt;
	public GameObject myComputer;
	public GameObject DVDicon;
	public GameObject DVDmenu;
	public GameObject DVDeject;

	public AudioSource rainyBGM;
	public AudioSource creepyBGM;

	int count = 0;

	// Use this for initialization
	void Start () {
		head.SetActive (false);

		desktop.SetActive (false);
		icon.SetActive (false);
		txt.SetActive (false);
		myComputer.SetActive (false);
		DVDicon.SetActive (false);
		DVDmenu.SetActive (false);
		DVDeject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void start () {
		if (pwd.text == "Emily1216") {
			login.SetActive (false);
			pwd_object.SetActive (false);
			enter.SetActive (false);
			head.SetActive (false);

			desktop.SetActive (true);
			icon.SetActive (true);
			myComputer.SetActive (true);

		} else if (count < 2) {
			pwd.text = "";
			count++;
		} else {
			head.SetActive (true);
			/* BGM here */
			bgmSource.PlayOneShot (bgmSource.clip, 1F);
			count = 0;
		}
	}
}
