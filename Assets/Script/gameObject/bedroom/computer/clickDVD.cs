﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class clickDVD : MonoBehaviour {

	public GameObject display;
	public GameObject icon;
	public GameObject menu;
	public GameObject eject;
	public GameObject flag;

	// Use this for initialization
	void Start () {
		display.SetActive (false);
		icon.SetActive (false);
	}

	// Update is called once per frame
	void Update() {
		
		if( Input.GetMouseButtonDown(0) && flag.activeSelf ) {
			display.SetActive (false);
			icon.SetActive (false);
			menu.SetActive (false);
			eject.SetActive (false);
		}
	}

	public void start () {
		Debug.Log ("Pressed left click!!!.");
		display.SetActive (true);
		icon.SetActive (true);
	}
		
}
