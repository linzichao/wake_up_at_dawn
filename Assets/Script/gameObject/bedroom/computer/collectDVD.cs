﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collectDVD : MonoBehaviour {

	public GameObject DVD;
	public GameObject DVD_display;
	public GameObject DVD_icon;
	public GameObject DVD_menu;
	public GameObject DVD_eject;
    public GameObject noDVDHintText;
	public GameObject flag;

	private int completed;

	// Use this for initialization
	void Start () {
		completed = 0;
	}

	// Update is called once per frame
	void Update() {
		if ( Input.GetKey(KeyCode.Escape) ) {
			DVD_display.SetActive (false);
			DVD_icon.SetActive (false);
			DVD_menu.SetActive (false);
			DVD_eject.SetActive (false);
		}
	}

	public void start () {
		Debug.Log ("Pressed left click!!!.");
		DVD.SetActive (true);
		flag.SetActive (true);
        Destroy(noDVDHintText);
		completed = 1;

	}
}