﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clickIcon : MonoBehaviour {

	public GameObject txt;

	// Use this for initialization
	void Start () {
		txt.SetActive (false);
	}

	// Update is called once per frame
	void Update() {
		if(Input.GetMouseButtonDown(0)) {
			if(txt.activeSelf == true) {
				txt.SetActive (false);
			}
		}
	}

	public void start () {
		Debug.Log ("Pressed left click!!!.");
		txt.SetActive (true);
	}
}
