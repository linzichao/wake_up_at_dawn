﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class triggerCloset : MonoBehaviour {
	
	public GameObject hintText;
	public GameObject flag;
	public GameObject pill;
	public GameObject description;
	public GameObject gotHint;
	bool spacePressed;

	public Flowchart closetFlowchart;



	// Use this for initialization
	private void OnTriggerEnter(Collider other)
	{
		if( other.tag=="Player" && !hintText.activeSelf)
		{
			spacePressed = false;
			hintText.SetActive(true);

		}
	}
	private void OnTriggerStay(Collider other)
	{
		if (other.tag == "Player" && !spacePressed && Input.GetKey(KeyCode.Space))
		{
			spacePressed = true;
			hintText.SetActive(false);

			if (flag.activeSelf) {
				Block targetBlock = closetFlowchart.FindBlock("closetDialog2");
				closetFlowchart.ExecuteBlock(targetBlock);

				gotHint.SetActive (true);

				pill.SetActive (true);
				description.SetActive (false);
			} else {

				Block targetBlock = closetFlowchart.FindBlock("closetDialog1");
				closetFlowchart.ExecuteBlock(targetBlock);
			}
				
		}
	}
	private void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player" && hintText.activeSelf)
		{
			hintText.SetActive(false);
		}

		if (other.tag == "Player" && gotHint.activeSelf)
		{
			gotHint.SetActive(false);
		}


	}
}
