﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class openDownDoor : MonoBehaviour
{

    public GameObject hintText;

    public GameObject colliderObject;
    public Collider doorCollider;


    // Use this for initialization
    void Start()
    {
        doorCollider = colliderObject.GetComponent<Collider>();

    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            hintText.SetActive(true);
        }
    }
    public void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && Input.GetKey(KeyCode.Space))
        {
            hintText.SetActive(false);
            other.gameObject.transform.position = new Vector3(-160, 12.5f, -5);
            Debug.Log("correct");
            doorCollider.enabled = false;
        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            hintText.SetActive(false);
        }
    }
    void Update()
    {

        if (!doorCollider.enabled)
        {
            doorCollider.enabled = true;
        }

    }
}
