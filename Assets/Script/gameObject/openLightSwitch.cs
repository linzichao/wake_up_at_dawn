﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class openLightSwitch : MonoBehaviour {
    public GameObject hintText;
    public GameObject lightSwitch;
    public Vector3 closedAngle;
    public Vector3 openedAngle;
    public GameObject lamp;
    public GameObject key;
    bool spacePressed;
    bool isLit = false;
    // Use this for initialization
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !hintText.activeSelf)
        {
            spacePressed = false;
            hintText.SetActive(true);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && !spacePressed && Input.GetKey(KeyCode.Space))
        {
            spacePressed = true;
            hintText.SetActive(false);

            isLit = !isLit;
            lamp.SetActive(isLit);
            if(key!=null) key.SetActive(isLit);
            lamp.transform.localEulerAngles = isLit? openedAngle : closedAngle;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" && hintText.activeSelf)
        {
            hintText.SetActive(false);
        }
    }
}
