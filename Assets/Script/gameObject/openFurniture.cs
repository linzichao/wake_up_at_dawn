﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class openFurniture : MonoBehaviour {
    public GameObject openedCanvas;
    public GameObject hintText;
    bool spacePressed;
    // Use this for initialization
    private void OnTriggerEnter(Collider other)
    {
        if( other.tag=="Player" && !hintText.activeSelf)
        {
            spacePressed = false;
            hintText.SetActive(true);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && !spacePressed && Input.GetKey(KeyCode.Space))
        {
            spacePressed = true;
            hintText.SetActive(false);
            openedCanvas.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" && hintText.activeSelf)
        {
            hintText.SetActive(false);
        }
        if (other.tag == "Player" && openedCanvas.activeSelf)
        {
            openedCanvas.SetActive(false);
        }
    }
}
