﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class usePlunger : MonoBehaviour {
    public GameObject openCanvas;
    public Animator plungerAnimator;
    public string plungerAnimatorVariable;
    public Animator waterAnimator;
    public string waterAnimatorVariable;
    public GameObject hintText;
    bool spacePressed;
    // Use this for initialization
    private void OnTriggerEnter(Collider other)
    {
        if( other.tag=="Player" && !hintText.activeSelf)
        {
            spacePressed = false;
            hintText.SetActive(true);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && !spacePressed && Input.GetKey(KeyCode.Space))
        {
            spacePressed = true;
            hintText.SetActive(false);
            openCanvas.SetActive(true);
            //plungerAnimator.SetBool(plungerAnimatorVariable, true);
            //waterAnimator.SetBool(waterAnimatorVariable, true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" && hintText.activeSelf)
        {
            hintText.SetActive(false);
        }
    }
}
