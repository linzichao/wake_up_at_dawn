﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rejectEnterWater : MonoBehaviour {
    public Vector3 moveBackDisplacement;
    public GameObject dialog;
    public GameObject player;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter(Collider other)
    {
        //other.transform.position += moveBackDisplacement;
        player.GetComponent<moveAndRotate>().enabled = false;
        dialog.SetActive(false);
        dialog.SetActive(true);
    }
    public void moveBack()
    {
        player.transform.position += moveBackDisplacement;
    }
    private void OnTriggerExit(Collider other)
    {
        player.GetComponent<moveAndRotate>().enabled = true;
    }
}
