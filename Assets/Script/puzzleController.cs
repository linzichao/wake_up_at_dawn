﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puzzleController : MonoBehaviour {

	public GameObject b1;
	public GameObject b2;
	public GameObject b3;
	public GameObject b4;
	public GameObject b5;
	public GameObject b6;
	public GameObject b7;
	public GameObject b8;
	public GameObject b9;

	public GameObject description;
	public GameObject hint;
	public GameObject flag;

	GameObject[] arr;

	// Use this for initialization
	void Start () {
		arr = new GameObject[] { b1, b2, b3, b4, b5, b6, b7, b8, b9 };
		hint.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void judge() {
		bool solved = true;
		for (int i = 0; i < 9; i++) {
			if (arr [i].transform.rotation.z == 0) {
				if (arr [i].transform.localScale != new Vector3 (1, 1, 1)) {
					solved = false;
					Debug.Log ("round " + i + " Not solved 1 !!");
					Debug.Log ("scale.z == " + arr [i].transform.rotation.z);
					break;
				}
			} 
			else if (arr [i].transform.rotation == new Quaternion(0, 0, 1, 0) || arr [i].transform.rotation == new Quaternion(0, 0, -1, 0)) {
				if (arr [i].transform.localScale != new Vector3 (-1, -1, 1)) {
					solved = false;
					Debug.Log ("round " + i + " Not solved 2 !!");
					Debug.Log ("scale.z == " + arr [i].transform.rotation.z);
					break;
				}
			} 
			else {
				solved = false;
				Debug.Log ("round " + i + " Not solved 3 !!");
				Debug.Log ("scale.z == " + arr [i].transform.rotation.z);
				break;
			}
		}

		if (solved) {
			Debug.Log ("Puzzle solved!!");
			description.SetActive (true);
			flag.SetActive (true);
			hint.SetActive (true);
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player" && hint.activeSelf)
		{
			hint.SetActive(false);
		}
	}
}
