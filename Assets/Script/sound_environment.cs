﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sound_environment : MonoBehaviour {

	public AudioClip voice;
	public AudioSource audio;

	// Use this for initialization
	void Start() {
		//audio = GetComponent<AudioSource>();
		audio.volume = 1.0f;
		InvokeRepeating("PlayVoice", 5.0f, 120f);
	}

	public void PlayVoice() {
		audio.PlayOneShot(voice);
	}
}
