﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class muteBGM : MonoBehaviour {

	public AudioSource rainyBGM;
	public AudioSource creepyBGM;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision c) {
		rainyBGM.volume = 0.2f;
		creepyBGM.volume = 0.2f;
	}

	void OnCollisionExit(Collision c) {
		rainyBGM.volume = 1;
		creepyBGM.volume = 1;
	}
}
