﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackToBase : MonoBehaviour {

	public GameObject hintText;

	public GameObject rotateObject;
	public GameObject colliderObject;
	public Collider doorCollider;

	public float closedAngle;
	public float openedAngle;
	public float rotateSpeed;

	public float v1;
	public float v2;
	public float v3;

	float goalAngle;

	// Use this for initialization
	void Start()
	{
		doorCollider = colliderObject.GetComponent<Collider>();
		goalAngle = rotateObject.transform.localEulerAngles.y;
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player")
		{
			hintText.SetActive(true);
		}
	}
	private void OnTriggerStay(Collider other)
	{
		if (other.tag == "Player" && Input.GetKey(KeyCode.Space) && hintText.activeSelf)
		{
			hintText.SetActive(false);
			other.gameObject.transform.position = new Vector3(v1,v2,v3);

			Debug.Log("in");
		}else if(other.tag == "Player" && Input.GetKey(KeyCode.Space) && hintText.activeSelf){
			hintText.SetActive(false);
		}
	}
	private void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player")
		{
			if (!hintText.activeSelf) 
			{ 
				goalAngle = closedAngle;
			}
			else 
			{ 
				hintText.SetActive(false);
			}
		}
	}
	void Update()
	{
		Vector3 objectAngle = rotateObject.transform.localEulerAngles;
		if (Mathf.Abs(objectAngle.y - goalAngle) > 0.1)
		{
			//sound there
			float ang = rotateSpeed * (goalAngle - objectAngle.y > 0 ? 1 : -1);
			rotateObject.transform.localEulerAngles = new Vector3(objectAngle.x, objectAngle.y + ang, objectAngle.z);
		}
		else if (!doorCollider.enabled)
		{
			doorCollider.enabled = true;
		}
	}
}
