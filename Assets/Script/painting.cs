﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;



public class painting : MonoBehaviour ,IPointerClickHandler {

	public GameObject controller;
	public AudioClip clip;
	public AudioSource source;
	private int count = 0;

	//Image image;
	//public Image sprite;



	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	/*
	public void start () {
		Debug.Log("You have clicked the button2!");
		this.transform.Rotate (new Vector3 (0, 0, 90));

		controller.GetComponent<puzzleController>().judge();
	} */


	public void OnPointerClick(PointerEventData eventData)
	{
		source.PlayOneShot (clip, 1);

		if (eventData.button == PointerEventData.InputButton.Left) {
			this.transform.Rotate (new Vector3 (0, 0, 90));
			count = (count + 1) % 4;
		} 
		else if (eventData.button == PointerEventData.InputButton.Right) {

			if(count == 0 || count == 2) {
				this.transform.localScale = new Vector3(this.transform.localScale.x * -1, this.transform.localScale.y, this.transform.localScale.z);
			}
			if(count == 1 || count == 3) {
				this.transform.localScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y * -1, this.transform.localScale.z);
			}
		}

		controller.GetComponent<puzzleController>().judge();
	}

}