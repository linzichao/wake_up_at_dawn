﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using UnityEngine.EventSystems;

public class ButtonColorController : MonoBehaviour {

	public Button button;

	public Color Color0; // black
	public Color Color1; // dark gray
	public Color Color2; // gray
	public Color Color3; // white


	// Use this for initialization
	void Start () {
		ColorBlock cb = button.colors;
		cb.normalColor = Color0;
		cb.pressedColor = Color0;
		button.colors = cb;
	}
	
	// Update is called once per frame
	void Update () {
		if (button.GetComponentInChildren<Text> ().text == "0") {
			ColorBlock cb = button.colors;
			cb.normalColor = Color0;
			cb.pressedColor = Color0;
			//cb.highlightedColor = Color2;
			button.colors = cb;
		}
		else if (button.GetComponentInChildren<Text> ().text == "1") {
			ColorBlock cb = button.colors;
			cb.normalColor = Color1;
			cb.pressedColor = Color1;
			//cb.highlightedColor = Color2;
			button.colors = cb;
		}
		else if (button.GetComponentInChildren<Text> ().text == "2") {
			ColorBlock cb = button.colors;
			cb.normalColor = Color2;
			cb.pressedColor = Color2;
			//cb.highlightedColor = Color2;
			button.colors = cb;
		}
		else if (button.GetComponentInChildren<Text> ().text == "3") {
			ColorBlock cb = button.colors;
			cb.normalColor = Color3;
			cb.pressedColor = Color3;
			//cb.highlightedColor = Color2;
			button.colors = cb;
		}
	}

}
