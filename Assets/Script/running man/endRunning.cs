﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;


public class endRunning : MonoBehaviour {
    public Flowchart flowChart;
    public GameObject peepHole;
    public GameObject darkOutSide;
    public GameObject gameManager;
	public GameObject player;
	public GameObject bg;
	public GameObject light;
	public GameObject ghost;
	public GameObject dialog;


    private void OnEnable()
    {
        peepHole.SetActive(false);
        darkOutSide.SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        end();
    }
    public void end()
    {
        Block targetBlock = flowChart.FindBlock("set success");
        flowChart.ExecuteBlock(targetBlock);

    }

	public void endingStory() {
		player.SetActive (true);
		bg.SetActive (false);
		light.SetActive (true);
		ghost.SetActive (false);
		dialog.SetActive (true);

		player.transform.position = new Vector3 (-93.64f, 2, 119f);
		player.transform.rotation = new Quaternion(-0.348f, 1, 0, 0);
	}
}
