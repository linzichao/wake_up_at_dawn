﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enableRunning : MonoBehaviour {
    public GameObject flowChart;
    public Collider endTrigger;
    private void OnTriggerEnter(Collider other)
    {
        flowChart.SetActive(true);
        endTrigger.gameObject.SetActive(true);
    }
}
