﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sound : MonoBehaviour {

	public AudioClip voice;
	public AudioSource audio;

	// Use this for initialization
	void Start() {
		//audio = GetComponent<AudioSource>();
		audio.volume = 1.0f;
		InvokeRepeating("PlayVoice", 15.0f, 60f);
	}

	public void PlayVoice() {
		audio.PlayOneShot(voice);
	}
}
