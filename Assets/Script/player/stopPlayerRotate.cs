﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stopPlayerRotate : MonoBehaviour {
    
    public cameraMove script;

    private void OnEnable()
    {
        script.enabled = false;
    }
    private void OnDisable()
    {
        script.enabled = true;
    }
}
