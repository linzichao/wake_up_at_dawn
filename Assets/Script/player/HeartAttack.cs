﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HeartAttack : MonoBehaviour {

    public GameObject canvasPrefab;

    public GameObject bone1;
    public GameObject bone2;
    public GameObject bone3;

    public AudioSource scary;

    public static int bone = 0;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "deadbody"){

            Collider coll = collision.gameObject.GetComponent<Collider>();
            coll.enabled = false;
            pain();
            scary.Play();
            bone++;
            heart_attack();

        }

    }

    public void heart_attack()
    {

        if (bone > 0 )
        {
            bone1.SetActive(true);
        }
        if (bone > 1 )
        {
            bone2.SetActive(true);
        }
        if (bone > 2 )
        {
            bone3.SetActive(true);
        }
    
    }


    IEnumerator timer()
    {

        yield return new WaitForSeconds(0.3f);
        canvasPrefab.SetActive(false);

    }


    /// <summary>
    /// 受傷紅屏
    /// </summary>
    public void pain()
    {

        canvasPrefab.SetActive(true);
        StartCoroutine(timer());

    }

}
