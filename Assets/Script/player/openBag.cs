﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class openBag : MonoBehaviour {

    public GameObject bag;
    public GameObject objectCanvas;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if( Input.GetKey(KeyCode.F1) && !bag.activeSelf)
        {
            bag.SetActive(true);
        }
        else if( Input.GetKey(KeyCode.Escape))
        {
            bag.SetActive(false);
            objectCanvas.SetActive(false);
        }
	}
}
