﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class moveAndRotate : MonoBehaviour {

    public float moveSpeed;
    public float rotateSpeed;
    public GameObject body;
    public static Flowchart flowchartManager;

	float moveSpeedBuffer;
	float rotSpeedBuffer;
	
    // Use this for initialization
	void Start () {
        if (!body) body = gameObject;
        flowchartManager = GameObject.Find("FlowchartManager").GetComponent<Flowchart>();
	}

    // Update is called once per frame
    void FixedUpdate() {
        if (!isTalking)
        {
            if (Input.GetKey(KeyCode.W))
            {
                body.transform.Translate(new Vector3(0, 0, moveSpeed));
            }
            if (Input.GetKey(KeyCode.S))
            {
                body.transform.Translate(new Vector3(0, 0, -moveSpeed));
            }
            body.transform.Rotate(0, Input.GetAxis("Mouse X") *rotateSpeed, 0);
            //if (Input.GetKey(KeyCode.LeftArrow))
            //{
            //    body.transform.localEulerAngles += new Vector3(0, -rotateSpeed, 0);
            //}
            //if (Input.GetKey(KeyCode.RightArrow))
            //{
            //    body.transform.localEulerAngles += new Vector3(0, rotateSpeed, 0);
            //}
        }
    }

    // Flowchart control
    public static bool isTalking
    {
        get { return flowchartManager.GetBooleanVariable("InDialog"); }
    }

	public  void disableMove() {
		moveSpeedBuffer = moveSpeed;
		rotSpeedBuffer = rotateSpeed;

		moveSpeed = 0;
		rotateSpeed = 0;
	}

	public void enableMove() {
		moveSpeed = moveSpeedBuffer;
		rotateSpeed = rotSpeedBuffer;
	}

    public void setPosition(Vector3 pos)
    {
        gameObject.transform.position = pos;
    }

}
