﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraMove : MonoBehaviour
{
    int screenHeight;
    int screenCenter;

    // Use this for initialization
    void Start()
    {
        screenHeight = Screen.height;
        screenCenter = screenHeight / 2;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float theta = getVerticalThita(screenHeight, screenCenter, Input.mousePosition.y);
        float degree = Mathf.Rad2Deg * theta;
        Vector3 angle = gameObject.transform.localEulerAngles;
        gameObject.transform.localEulerAngles = new Vector3(-degree, angle.y, angle.z);
    }
    private float getVerticalThita(float top, float center, float mouse)
    {
        float r = top - center;
        float y = mouse - center;
        float sin = y / r;
        if (sin > 1) sin = 1;
        if (sin < -1) sin = -1;
        return Mathf.Asin(sin);
    }
}
