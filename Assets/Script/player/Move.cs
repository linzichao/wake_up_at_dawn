﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

    public GameObject cube;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if(Input.GetKeyUp(KeyCode.UpArrow)){

            cube.transform.Translate(new Vector3(0,0, 0.1f));

        }

        if (Input.GetKeyUp(KeyCode.DownArrow))
        {

            cube.transform.Translate(new Vector3(0, 0, -0.1f));

        }

        if(Input.GetKeyUp(KeyCode.LeftArrow)){

            cube.transform.Translate(new Vector3(-0.1f, 0, 0));

        }

        if (Input.GetKeyUp(KeyCode.RightArrow))
        {

            cube.transform.Translate(new Vector3(0.1f, 0, 0));

        }
	
    }
}
